class Area < ApplicationRecord
  has_many :sub_areas, class_name: "Area", foreign_key: "parte_de"
  belongs_to :parte_de, class_name: "Area", optional: true
  has_many :documento_tiene_area
  has_many :documentos, through: :documento_tiene_area

  validates :nombre, presence: true

  def full_name
    if parte_de.present?
      nombre + ' (' + parte_de.nombre + ')'
    else
      nombre
    end
  end
end

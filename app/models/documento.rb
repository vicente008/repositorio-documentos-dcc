class Documento < ApplicationRecord
  belongs_to :tipo_documento
  belongs_to :user
  has_many :documento_tiene_areas
  has_many :areas, through: :documento_tiene_areas
  has_many :documento_tiene_tags
  has_many :tags, through: :documento_tiene_tags
  has_many :user_descarga_documentos
  has_many :users_que_han_descargado, through: :user_descarga_documentos, source: :user
  mount_uploader :archivo_documento, DocumentoUploader
  validates_integrity_of :archivo_documento
  has_paper_trail
  validates :titulo, presence: true
  validates :archivo_documento, presence: true
  validates :tipo_documento_id, presence: true
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise
  has_many :documentos
  has_many :favoritos
  has_many :documentos_favoritos, -> { order 'favoritos.id desc' }, through: :favoritos, source: :documento
  has_many :user_descarga_documentos
  has_many :documentos_descargados, -> { order 'user_descarga_documentos.id desc'}, through: :user_descarga_documentos, source: :documento

  enum permiso: [:Sin_Permisos, :Visitante, :Asistente, :Coordinador, :Administrador]

  def puede_ver_sitio?
    self.Visitante? or self.Asistente? or self.Coordinador? or self.Administrador?
  end

  def puede_modificar_data?
    self.Asistente? or self.Coordinador? or self.Administrador?
  end

  def puede_subir_archivos?
    self.Asistente? or self.Coordinador? or self.Administrador?
  end

  def puede_eliminar_archivos?
    self.Coordinador? or self.Administrador?
  end

  def puede_sobreescribir_archivos?
    self.Coordinador? or self.Administrador?
  end

  def puede_administrar?
    self.Administrador?
  end
end

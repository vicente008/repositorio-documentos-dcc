class Tag < ApplicationRecord
  has_many :documentos, through: :documento_tiene_tag
end

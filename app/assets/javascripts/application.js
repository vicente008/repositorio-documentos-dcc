// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3

//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require font_awesome5
//= require moment
//= require_tree .

(function($){
	$.fn.datepicker['dates']['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy"
	};
}(jQuery));

$.fn.datepicker.defaults.format = "dd/mm/yyyy";
$.fn.datepicker.defaults.language = "es";
$.fn.datepicker.defaults.weekStart = 1;
$.fn.datepicker.defaults.clearBtn = true;
$.fn.datepicker.defaults.daysOfWeekHighlighted = 0;
$.fn.datepicker.defaults.todayHighlight = true;
$.fn.datepicker.defaults.autoclose = true;

var dataTable_locale = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ documentos",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando resultados _START_ al _END_. Se encontró _TOTAL_ documentos",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Filtrar Contenido:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
window.dataTable_locale = dataTable_locale
var dataTable_users_locale = JSON.parse(JSON.stringify(window.dataTable_locale));
dataTable_users_locale["sInfo"] = "Mostrando usuarios del _START_ al _END_. Se encontró _TOTAL_ usuarios."
dataTable_users_locale["sLengthMenu"] = "Mostrar _MENU_ usuarios"

var dataTable_tipos_locale = JSON.parse(JSON.stringify(window.dataTable_locale));
dataTable_tipos_locale["sInfo"] = "Mostrando tipos del _START_ al _END_. Se encontró _TOTAL_ tipos."
dataTable_tipos_locale["sLengthMenu"] = "Mostrar _MENU_ tipos"

var dataTable_areas_locale = JSON.parse(JSON.stringify(window.dataTable_locale));
dataTable_areas_locale["sInfo"] = "Mostrando áreas de la _START_ a la _END_. Se encontró _TOTAL_ áreas."
dataTable_areas_locale["sLengthMenu"] = "Mostrar _MENU_ áreas"

var dataTable_logs_locale = JSON.parse(JSON.stringify(window.dataTable_locale));
dataTable_logs_locale["sInfo"] = "Mostrando registros de la _START_ a la _END_ de un total de _TOTAL_ registros."
dataTable_logs_locale["sLengthMenu"] = "Mostrar _MENU_ registros"

window.dataTable_users_locale = dataTable_users_locale;
window.dataTable_tipos_locale = dataTable_tipos_locale;
window.dataTable_areas_locale = dataTable_areas_locale;





//ADMIN.js
$(function() {
    $('a[data-toggle="tab"]').on('click', function(e) {
        window.localStorage.setItem('activeTab', $(e.target).attr('href'));
        console.log(activeTab)
    });
    var activeTab = window.localStorage.getItem('activeTab');
    if (activeTab) {
        $('#v-pills-tab a[href="' + activeTab + '"]').tab('show');
    }
});

function filter(e) {
  // Declare variables
  console.log($('#'+e.id))
  let tableName = e.attributes.tablename.nodeValue;
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById(tableName+"TableFilter");
  filter = input.value.toUpperCase();
  table = document.getElementById(tableName+"Table");
  tbody = table.getElementsByTagName("tbody");
  tr = tbody[0].rows
  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}



//CLICKABLE-ROW
function clickableRowJS(){
	console.log("clickablerowjs")
	$(".not-clickable").click(function(){

	})
	$(".clickable").click(function() {
		window.location = $(this).parent().data("href");
	}).css( 'cursor', 'pointer' );
}
$(document).ready(function(){clickableRowJS()})
document.addEventListener("turbolinks:load", clickableRowJS)


//DOCUMENTOS.js

// Add the following code if you want the name of the file appear on select
function relabelInput(e){
  console.log(e)
  var fileName = $("#"+e.id).val().split("\\").pop();
  $("#"+e.id).siblings(".custom-file-label").addClass("selected").html(fileName);
}
// In your Javascript (external .js resource or <script> tag)
function loadDocumentosJS(){
		console.log("documentos.js")
    $('.js-example-basic-single').select2({
      placeholder: "Buscar tipos"
    });
    $('.js-example-basic-multiple').select2({
      placeholder: "Buscar áreas"
    });
    $('.fav-link').on('click', function () {
        var doc_id = window.location.pathname.split("/")[2]
        if($('.fav-link').hasClass('fav-link-unmarked')){
          $.post( "/add_favorito", { id:doc_id }).done(function(data){
            if(data.ok){
              $('.fav-link-container').html("Agregado a Favoritos !")
              $('.fav-button').removeClass('btn')
              $('.fav-button').removeClass('btn-light')
              $('.fav-button').addClass('fav-added')
            }
          });
        } else {
          $.post( "/remove_favorito", { id:doc_id }).done(function(data){
            if(data.ok){
              $('.fav-link-container').html("Eliminado de Favoritos !")
              $('.fav-button').removeClass('btn')
              $('.fav-button').removeClass('btn-light')
              $('.fav-button').addClass('fav-added')
            }
          });
        }
    });
}
$(document).ready(loadDocumentosJS())
document.addEventListener("turbolinks:load", loadDocumentosJS)


//HOME.js
function filterTipos() {
  // Declare variables
  var input;
  input = $('#exploradorTipos').val().toLowerCase()
  var paneles = $('.container-exploracion-tipos').find('.panel-exploracion')
  for(i = 0; i<paneles.length; i++){
    var span = $(paneles[i]).find('span')[0]
    var texto = $(span).text().toLowerCase()
    if(input == '' || texto.includes(input)){
      paneles[i].style.display = "inline-block";
    } else {
      paneles[i].style.display = "none";
    }
  }
}

function filterAreas() {
  // Declare variables
  var input;
  input = $('#exploradorAreas').val().toLowerCase()
  var paneles = $('.container-exploracion-areas').find('.panel-exploracion')
  for(i = 0; i<paneles.length; i++){
    var span = $(paneles[i]).find('span')[0]
    var texto = $(span).text().toLowerCase()
    if(input=='' || texto.includes(input)){
      paneles[i].style.display = "inline-block";
    } else {
      paneles[i].style.display = "none";
    }
  }
}

function loadHomeJS(){
	console.log("home.js")
	$('#tipos-form').on('submit', function(e){
			filterTipos();
			e.preventDefault();
	});
	$('#areas-form').on('submit', function(e){
			filterAreas();
			e.preventDefault();
	});
}

$(document).ready(loadHomeJS())
document.addEventListener("turbolinks:load", loadHomeJS)

class AdminController < ApplicationController
  before_action :permiso_acceso
  def index
    @tipos = TipoDocumento.all
    @areas = Area.all
    @permisos = User.permisos
    @logs = PaperTrail::Version.all
    @descargas = UserDescargaDocumento.all
    @users = User.all
    # render plain: @users.to_json
  end


  def create_tipo
    #render plain: path and return
    @nuevo_tipo = TipoDocumento.new
    @nuevo_tipo.nombre = params[:nombre]
    @nuevo_tipo.created_at = DateTime.now
    @nuevo_tipo.updated_at = DateTime.now
    if @nuevo_tipo.save
      flash[:notice] = "Tipo creado con éxito"
    else
      alert = "ERROR"
    end
    redirect_to '/admin/tipos'
  end

  def update_tipo
    @tipo_a_editar = TipoDocumento.find(params[:id])
    @tipo_a_editar.nombre = params[:nombre]
    @tipo_a_editar.updated_at = DateTime.now
    @tipo_a_editar.save
    redirect_to '/admin/tipos'
  end

  def delete_tipo
    @tipo_a_eliminar = TipoDocumento.find(params[:id])
    begin
      @tipo_a_eliminar.destroy
    rescue ActiveRecord::InvalidForeignKey
      flash[:alert] = "Hubo un error eliminando el Tipo: aún hay documentos asociados al tipo seleccionado"
    end
    redirect_to '/admin/tipos' and return
  end

  def create_area
    if(params[:parte_de_id] == "")
      @nueva_area = Area.new
    else
      @area_padre = Area.find(params[:parte_de_id])
      @nueva_area = @area_padre.sub_areas.new
    end
    @nueva_area.nombre = params[:nombre]
    @nueva_area.created_at = DateTime.now
    @nueva_area.updated_at = DateTime.now
    @nueva_area.save
    redirect_to '/admin/areas'
  end

  def update_area
    @area_a_editar = Area.find(params[:id])
    if (params[:nombre] != "")
      @area_a_editar.nombre=params[:nombre]
    end
    if(params[:parte_de_id])
      if @area_a_editar.id != params[:parte_de_id] and not @area_a_editar.sub_areas.present?
        @area_a_editar.parte_de_id=params[:parte_de_id]
      end
    end
    @area_a_editar.save
    redirect_to '/admin/areas' and return
  end

  def delete_area
    @area_a_eliminar = Area.find(params[:id])
    if(@area_a_eliminar.sub_areas.size > 0)
      # render json: @area_a_eliminar.sub_areas.to_json and return
      @area_a_eliminar.sub_areas.each do |relationship|
        relationship.documento_tiene_area.each do |document_subarea_relationship|
          document_subarea_relationship.destroy
        end
        relationship.destroy
      end
      flash[:alert] = "Se eliminó correctamente el área y sus subáreas."
    end
    @area_a_eliminar.documento_tiene_area.each do |relationship|
      relationship.destroy
    end
    @area_a_eliminar.destroy
    redirect_to '/admin/areas' and return
  end  



  def update_usuario
    @user = User.find(params[:id])
    @user.permiso = params[:permiso]
    @user.save
    redirect_to action: 'index'
  end

  private def permiso_acceso
    if not current_user.puede_administrar?
      flash[:alert] = "No tienes permisos para acceder a esta sección."
      redirect_to root_url and return
    end
  end
end

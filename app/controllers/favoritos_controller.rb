class FavoritosController < ApplicationController
  def show
    @favoritos = current_user.documentos_favoritos.where("documentos.activo = true")
  end
end

class UserController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :externo
  @@log_in_placeholder_esta_habilidado = false

  def log_in
  end

  def log_out
    sign_out :user
    redirect_to user_log_in_path
  end

  def log_in_as_user
    if not Rails.env.development?
      redirect_to root_path
      return
    end
    tipo = params[:tipo]
    user = User.find_by nombre: "Usuario #{tipo}"
    if not user.present?
      user = User.new
      user.nombre = "Usuario #{tipo}"
      user.rut = "11.111.111-1"
      user.permiso = tipo
      user.save!
    end
    sign_in(user, scope: :user)
    if tipo == "Administrador"
      redirect_to admin_path
    else
      redirect_to root_path
    end
  end

  def sin_permisos
  end

  DATA_PROVIDER = 'https://www.u-cursos.cl/upasaporte/?servicio=repodcc&ticket=%s'
  REDIRECT_URL = 'http://docs.dcc.uchile.cl/user/authenticated?sessid='
  @@SESSIONS = {}

  def externo
    data = Net::HTTP.get(URI(sprintf(DATA_PROVIDER, params[:ticket])))
    if not data or data.length <= 2
      render :plain => 'Surgió un error ', :status => 400
      return
    end
    uuid = SecureRandom.uuid;
    @@SESSIONS[uuid] = data;
    redirect_url_to_return = REDIRECT_URL+uuid
    render plain: redirect_url_to_return
  end

  def authenticated
    raw_data = @@SESSIONS[params[:sessid]]
    if not raw_data.present?
      render :text => 'error', :status => 400
      return
    end
    user_data = JSON.parse(raw_data)
    rut = user_data["pers_id"]
    user = User.find_by rut: rut
    if not user.present?
      user = User.new
      user.nombre = user_data["alias"]
      user.rut = rut
      user.permiso = :Sin_Permisos
      user.save!
    end
    sign_in(user, scope: :user)
    @@SESSIONS.delete(params[:sessid])
    if user.permiso == :Administrador
      redirect_to admin_path
    else
      redirect_to root_path
    end
  end
end

class DocumentosController < ApplicationController
  before_action :permiso_subir_documento, only: [:new, :create]
  before_action :permiso_modificar_data, only: [:edit]
  before_action :permiso_eliminar_archivo, only: [:delete]
  before_action :permiso_sobreescribir_archivo, only: [:rewrite]
  before_action :documento_esta_activo_show, only: [:show]
  before_action :documento_esta_activo_accion, only: [:edit, :rewrite, :delete]

  def index
    redirect_to search_path+"?title="
  end

  def new
    @document = Documento.new
    @tipos = TipoDocumento.all
    @areas = Area.all
  end

  def show
    @document = Documento.find(params[:id])
    @tipo= TipoDocumento.find(@document.tipo_documento_id)
  end

  def descargar
    @documento = Documento.find(params[:data])
    if not @documento.activo
      flash[:alert] = "Este documento ya no está disponible."
      redirect_to root_path
    else
      ruta = @documento.archivo_documento
      descarga = UserDescargaDocumento.new
      descarga.user_id = current_user.id
      descarga.documento_id = params[:data]
      descarga.save!
      filename = Zaru.sanitize! @documento.titulo
      if @documento.extension.present?
        filename = filename + "." + Zaru.sanitize!(@documento.extension)
      end
      if not filename.present?
        filename = "file"
      end
      send_file ruta.path, filename: filename
    end
  end

  def create
    @user = current_user
    @document = Documento.new
    @document.user_id = @user.id
    @document.titulo=params[:title]
    @document.descripcion=params[:descripcion]
    @document.autor = params[:autor]
    begin
      @document.fecha_documento = DateTime.strptime(params[:fecha], '%d/%m/%Y')
    rescue
    end
    @document.fecha_subida = DateTime.now
    @document.created_at = DateTime.now
    @document.updated_at = DateTime.now
    @document.tipo_documento_id = params[:tipo]
    @areas_a_agregar = Area.where(id: params[:areas])
    @document.areas << @areas_a_agregar
    @document.archivo_documento = params[:file]
    @document.peso = File.size(params[:file].tempfile)
    if params[:file].content_type == "application/octet-stream"
      ext = ""
    else
      ext = Rack::Mime::MIME_TYPES.invert[params[:file].content_type]
      if not ext.present?
        ext = ""
      end
      begin
        ext = ext.gsub(".", "").upcase
      rescue        
      end
    end
    if ext.length > 50
      ext = ext[0..49]
    end
    @document.extension = ext
    if not @document.save
      flash[:alert] = "Error. Chequea el tamaño del archivo y los demás parámetros."
      redirect_to new_documento_path
    else
      redirect_to @document
    end
  end

  def edit
    @document = Documento.find(params[:document][:id])
    @document.update(document_params)
    @areas = params[:document][:area_ids]
    @document.update_attribute(:area_ids, @areas)
    redirect_to @document, notice: 'Documento exitosamente editado'
  end

  def rewrite
    old_id = params[:document][:id]
    new_file = params[:file]
    user = current_user
    old_doc = Documento.find(old_id)
    new_doc = Documento.new
    new_doc.titulo = old_doc.titulo
    new_doc.descripcion = old_doc.descripcion
    new_doc.autor = old_doc.autor
    new_doc.fecha_documento = old_doc.fecha_documento
    new_doc.tipo_documento = old_doc.tipo_documento
    new_doc.areas = old_doc.areas
    new_doc.user_id = user.id
    new_doc.archivo_documento = new_file
    new_doc.peso = File.size(new_file.tempfile)
    ext = Rack::Mime::MIME_TYPES.invert[params[:file].content_type]
    ext = ext.gsub(".", "").upcase
    if ext.length > 50
      ext = ext[0..49]
    end
    new_doc.extension = ext
    new_doc.fecha_subida = DateTime.now
    if new_doc.save
      old_doc.update_attributes(:activo => false, :sobreescrito => true )
      redirect_to new_doc, notice: 'Documento exitosamente actualizado'
    else
      flash[:alert] = "Error. Chequea el tamaño e integridad del archivo."
      redirect_to old_doc
    end
  end

  def delete
    document = Documento.find(params[:document][:id])
    document.update_attribute(:activo, false)
    redirect_to home_path, notice: 'Documento eliminado'
  end

  def add_favorite
    @favorito = Favorito.new
    @favorito.user_id = current_user.id
    @favorito.documento_id = params[:id]
    @favorito.created_at = DateTime.now
    @favorito.updated_at = DateTime.now
    @favorito.save
    render json: {ok:true}
  end

  def remove_favorite
    @favorito = Favorito.where(user_id: current_user.id, documento_id: params[:id]).take!
    @favorito.destroy
    render json: {ok:true}
  end

  private
    def document_params
      params.require(:document).permit(:titulo, :descripcion, :tipo_documento_id, :autor, :fecha_documento)
    end

    def permiso_subir_documento
      if not current_user.puede_subir_archivos?
        unauthorized
      end
    end
    def permiso_modificar_data
      if not current_user.puede_modificar_data?
        unauthorized
      end
    end
    def permiso_eliminar_archivo
      if not current_user.puede_eliminar_archivos?
        unauthorized
      end
    end
    def permiso_sobreescribir_archivo
      if not current_user.puede_sobreescribir_archivos?
        unauthorized
      end
    end
    def documento_esta_activo_show
      document = Documento.find(params[:id])
      if not document.activo
        flash[:alert] = "Este documento ya no está disponible."
        redirect_to root_path
      end
    end
    def documento_esta_activo_accion
      document = Documento.find(params[:document][:id])
      if not document.activo
        flash[:alert] = "Este documento ya no está disponible."
        redirect_to root_path
      end
    end
end

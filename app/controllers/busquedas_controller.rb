class BusquedasController < ApplicationController
  def advanced
    @areas = Area.all
    @tipos = TipoDocumento.all
  end
  def search
    search = params[:search]
    results = Documento.where(activo: true)
    results = results.where("titulo ILIKE ?", "%#{search[:title]}%")
    results = results.where("tipo_documento_id = ?", search[:tipo]) if !search[:tipo].blank?
    results = results.where("documentos.fecha_subida >= ?", search[:initial_date]) if !search[:initial_date].blank?
    results = results.where("documentos.fecha_subida <= ?", search[:end_date]) if !search[:end_date].blank?
    results = results.joins(:documento_tiene_areas).merge(DocumentoTieneArea.where("area_id = ?", search[:area])) if !search[:area].blank?
    @results = results
    @areas = Area.all
    @tipos = TipoDocumento.all
  end

  def normal_search
    results = Documento.where(activo: true)
    @results = results.where("titulo ILIKE ?", "%#{params[:title]}%")
  end
end

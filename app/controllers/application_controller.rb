class ApplicationController < ActionController::Base
  before_action :user_esta_log_in, except: [:log_in, :log_in_as_user, :externo, :authenticated]
  before_action :user_no_tiene_permisos, except: [:log_in, :log_in_as_user, :sin_permisos, :log_out, :externo, :authenticated]
  before_action :set_paper_trail_whodunnit, except: [:log_in, :log_in_as_user, :sin_permisos, :log_out, :externo, :authenticated]
  
  def unauthorized
    redirect_to root_path, alert: "No tienes los permisos para ver esa página. Redirigido a Inicio."
  end

  private
    def user_esta_log_in  
      if not user_signed_in?
        redirect_to user_log_in_path
      end
    end

    def user_no_tiene_permisos
      if current_user.present? and current_user.Sin_Permisos?
        redirect_to user_sin_permisos_path
      end
    end
end

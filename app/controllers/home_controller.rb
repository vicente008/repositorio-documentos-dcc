class HomeController < ApplicationController

  def index
    @areas = Area.all.order(:nombre)
    @tipos = TipoDocumento.all.order(:nombre)
    @favoritos = current_user.documentos_favoritos.where("documentos.activo = true").limit(5)
    descargas_ids = current_user.documentos_descargados.where(activo: true).order(created_at: :desc).limit(50).pluck(:id).uniq
    descargas_5 = descargas_ids[0..4]
    @descargas = Documento.find(descargas_5)
    @subidas = Documento.where(user: current_user, activo: true).order(created_at: :desc).limit(5)
  end

end

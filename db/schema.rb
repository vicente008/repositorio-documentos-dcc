# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_20_055901) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "nombre", limit: 50, null: false
    t.bigint "parte_de_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nombre"], name: "index_areas_on_nombre"
    t.index ["parte_de_id"], name: "index_areas_on_parte_de_id"
  end

  create_table "documento_tiene_areas", force: :cascade do |t|
    t.bigint "area_id"
    t.bigint "documento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_documento_tiene_areas_on_area_id"
    t.index ["documento_id"], name: "index_documento_tiene_areas_on_documento_id"
  end

  create_table "documento_tiene_tags", force: :cascade do |t|
    t.bigint "documento_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["documento_id"], name: "index_documento_tiene_tags_on_documento_id"
    t.index ["tag_id"], name: "index_documento_tiene_tags_on_tag_id"
  end

  create_table "documentos", force: :cascade do |t|
    t.bigint "tipo_documento_id"
    t.bigint "user_id"
    t.string "titulo", limit: 255, null: false
    t.text "descripcion"
    t.datetime "fecha_subida", null: false
    t.datetime "fecha_documento"
    t.bigint "peso", default: 0, null: false
    t.string "extension", limit: 50, null: false
    t.boolean "activo", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "archivo_documento"
    t.string "autor"
    t.boolean "sobreescrito", default: false
    t.index ["activo"], name: "index_documentos_on_activo"
    t.index ["fecha_documento"], name: "index_documentos_on_fecha_documento"
    t.index ["fecha_subida"], name: "index_documentos_on_fecha_subida"
    t.index ["tipo_documento_id"], name: "index_documentos_on_tipo_documento_id"
    t.index ["titulo"], name: "index_documentos_on_titulo"
    t.index ["user_id"], name: "index_documentos_on_user_id"
  end

  create_table "favoritos", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "documento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["documento_id"], name: "index_favoritos_on_documento_id"
    t.index ["user_id"], name: "index_favoritos_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "nombre", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_documentos", force: :cascade do |t|
    t.string "nombre", limit: 50, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nombre"], name: "index_tipo_documentos_on_nombre"
  end

  create_table "user_descarga_documentos", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "documento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["documento_id"], name: "index_user_descarga_documentos_on_documento_id"
    t.index ["user_id"], name: "index_user_descarga_documentos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nombre", limit: 100, null: false
    t.string "rut", limit: 20, null: false
    t.integer "permiso", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nombre"], name: "index_users_on_nombre"
    t.index ["permiso"], name: "index_users_on_permiso"
    t.index ["rut"], name: "index_users_on_rut"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "documento_tiene_areas", "areas"
  add_foreign_key "documento_tiene_areas", "documentos"
  add_foreign_key "documento_tiene_tags", "documentos"
  add_foreign_key "documento_tiene_tags", "tags"
  add_foreign_key "documentos", "tipo_documentos"
  add_foreign_key "documentos", "users"
  add_foreign_key "favoritos", "documentos"
  add_foreign_key "favoritos", "users"
  add_foreign_key "user_descarga_documentos", "documentos"
  add_foreign_key "user_descarga_documentos", "users"
end

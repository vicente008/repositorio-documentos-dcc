# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


u1 = User.create(nombre: "Víctor Garrido", rut: "19.246.252-6")
t1 = TipoDocumento.create(nombre: "Acta")
a1 = Area.create(nombre: "Pregrado")
a2 = Area.create(nombre: "Docencia", parte_de: a1)
a3 = Area.create(nombre: "Postgrado")
d1 = Documento.create(user: u1, tipo_documento: t1, titulo: "Acta 1", 
  descripcion: "Esta es una descripcion de prueba", fecha_subida: DateTime.now, fecha_documento: DateTime.now,
  peso: 1000, extension: "PDF", activo: true)
DocumentoTieneArea.create(documento: d1, area: a2)
DocumentoTieneArea.create(documento: d1, area: a3)
f1 = Favorito.create(user: u1, documento: d1)
class CreateDocumentoTieneAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :documento_tiene_areas do |t|
      t.references :area, foreign_key: true
      t.references :documento, foreign_key: true

      t.timestamps
    end
  end
end

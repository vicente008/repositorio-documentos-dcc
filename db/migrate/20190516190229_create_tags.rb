class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string :nombre, limit: 50

      t.timestamps
    end
  end
end

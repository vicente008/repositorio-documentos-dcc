class CreateDocumentos < ActiveRecord::Migration[5.2]
  def change
    create_table :documentos do |t|
      t.references :tipo_documento, foreign_key: true
      t.references :user, foreign_key: true
      t.string :titulo, limit: 50, null: false
      t.text :descripcion, limit: 1000
      t.datetime :fecha_subida, null: false
      t.datetime :fecha_documento, null: false
      t.bigint :peso, null: false, default: 0
      t.string :extension, limit: 20, null: false
      t.boolean :activo, null: false, default: true

      t.timestamps
    end
    add_index :documentos, :titulo
    add_index :documentos, :fecha_subida
    add_index :documentos, :fecha_documento
    add_index :documentos, :activo
  end
end

class AddAutorToDocumentos < ActiveRecord::Migration[5.2]
  def change
    add_column :documentos, :autor, :string
  end
end

class RemoveNullContstrainOnFechaCreacion < ActiveRecord::Migration[5.2]
  def change
    change_column :documentos, :fecha_documento, :datetime, :null => true
  end
end

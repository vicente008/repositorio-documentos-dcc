class CreateTipoDocumentos < ActiveRecord::Migration[5.2]
  def change
    create_table :tipo_documentos do |t|
      t.string :nombre, limit: 50, null: false

      t.timestamps
    end
    add_index :tipo_documentos, :nombre
  end
end

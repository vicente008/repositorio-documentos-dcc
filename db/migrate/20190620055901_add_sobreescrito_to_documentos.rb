class AddSobreescritoToDocumentos < ActiveRecord::Migration[5.2]
  def change
    add_column :documentos, :sobreescrito, :boolean, default: false
  end
end

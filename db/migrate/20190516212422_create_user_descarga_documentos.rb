class CreateUserDescargaDocumentos < ActiveRecord::Migration[5.2]
  def change
    create_table :user_descarga_documentos do |t|
      t.references :user, foreign_key: true
      t.references :documento, foreign_key: true

      t.timestamps
    end
  end
end

class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :areas do |t|
      t.string :nombre, limit: 50, null: false
      t.references :parte_de

      t.timestamps
    end
    add_index :areas, :nombre
  end
end

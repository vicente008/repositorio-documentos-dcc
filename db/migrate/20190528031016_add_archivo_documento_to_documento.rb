class AddArchivoDocumentoToDocumento < ActiveRecord::Migration[5.2]
  def change
    add_column :documentos, :archivo_documento, :string
  end
end

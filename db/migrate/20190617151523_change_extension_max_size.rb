class ChangeExtensionMaxSize < ActiveRecord::Migration[5.2]
  def up
    change_column :documentos, :extension, :string, :limit => 50
  end

  def down
    change_column :documentos, :extension, :string, :limit => 20
  end
end

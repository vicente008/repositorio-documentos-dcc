class CreateDocumentoTieneTags < ActiveRecord::Migration[5.2]
  def change
    create_table :documento_tiene_tags do |t|
      t.references :documento, foreign_key: true
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end

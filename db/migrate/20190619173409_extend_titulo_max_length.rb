class ExtendTituloMaxLength < ActiveRecord::Migration[5.2]
  def up
    change_column :documentos, :titulo, :string, :limit => 255
  end

  def down
    change_column :documentos, :titulo, :string, :limit => 50
  end
end

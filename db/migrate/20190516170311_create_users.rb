class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nombre, limit: 100, null: false
      t.string :rut, limit: 20, null: false, unique: true
      t.integer :permiso, null: false, default: 0

      t.timestamps
    end
    add_index :users, :nombre
    add_index :users, :rut
    add_index :users, :permiso
  end
end

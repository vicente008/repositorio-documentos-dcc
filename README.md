# README
## Links Útiles

### Ruby From Other Languages
https://www.ruby-lang.org/en/documentation/ruby-from-other-languages/

### Instalar Rails
https://gorails.com/setup/

### Partir con Rails
https://guides.rubyonrails.org/getting_started.html

## Base de datos
### Poblar
Para correr las migraciones: `rails db:migrate`  
Para reiniciar la base de datos: `rails db:reset`  
  
Para poblar la base de datos con ejemplos, se puede modificar el archivo "db/seeds.rb" agregando nuevas entradas, y luego se corre el comando `rails db:seed` para agregarlas todas a la base de datos a la vez. Ojo que si ya estaban en la base de datos se vuelven a agregar, por lo tanto es conveniente reiniciar la base de datos cada vez antes de hacer `rails db:seed`.  

### Asociaciones
Rails permite configurar asociaciones, que son basicamente conexiones de modelos. Al hacer una asociación, se agregan varios helpers que hacen mucho más agradable tratar con los objetos en la base de datos, de forma de minimizar las queries que hay que hacer.  
Pueden leer más aquí: https://guides.rubyonrails.org/association_basics.html  
Traté (Víctor) de dejar todas las asociaciones necesarias listas en la base de datos y en los modelos. Para usarlas, basta tomar un objeto y hacer el llamado correspondiente. Ej:  
```
user_ejemplo = User.first #toma el primer usuario de la base de datos
user_ejemplo.documentos_favoritos #retorna todos los documentos favoritos del usuario
user_ejemplo.documentos #retorna todos los documentos que ha subido el usuario
```

A continuación, dejo todas las asociaciones útiles que están en los modelos:  
user: documentos, documentos_favoritos, documentos_descargados  
tipo_documento: documentos  
tag: documentos  
documento: tipo_documento, user, areas, tags, users_que_han_descargado   

## Consola y debug
Para debuguear o simplemente ver como funciona realmente algo, se puede utilizar la consola de rails. Esta es una consola normal de Ruby, pero instanciada con todos los parámetros y datos del proyecto.    
Para lanzar la consola, usamos el comando `rails c`  
Acá un montón de info que les puede servir: https://pragmaticstudio.com/tutorials/rails-console-shortcuts-tips-tricks  
Alternativamente, para debuguear en un instante específico del código, se puede colocar `binding.pry` en cualquier lugar del código (modelo, vista, controlador). Cuando se llegue a esa línea, se pausa la ejecución, y en la consola donde está corriendo el servidor se abre una consola de rails, con todas las variables que se encuentran en el ambiente donde se llamó la línea. Para más info ver https://github.com/pry/pry

## Redes y U-Pasaporte
ssh repodoc@172.17.69.115 -p 122
ssh repodoc@gate.dcc.uchile.cl -p 822
https://github.com/Ucampus/upasaporte-server-demo

module PaperTrail
  class Version < ActiveRecord::Base
    include PaperTrail::VersionConcern
    def user
      User.find(self.whodunnit.to_i)
    end

    def document
      if item_type == 'Documento'
        Documento.find(item_id)
      else
        Documento.none
      end
    end
  end
end

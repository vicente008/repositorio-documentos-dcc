Rails.application.routes.draw do
  # usuarios y log in
  devise_for :users
  get 'user/log_in'
  get 'user/log_in_as_user'
  get 'user/log_out'
  get 'user/sin_permisos'
  post 'externo', to: 'user#externo'
  get 'user/authenticated'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'documentos/descargar', to: 'documentos#descargar'
  resources :documentos

  get 'favoritos', to: 'favoritos#show'
  post 'add_favorito', to: 'documentos#add_favorite'
  post 'remove_favorito', to: 'documentos#remove_favorite'

  get 'admin', to: 'admin#index'
  get 'admin/usuarios', to: 'admin#index', :as => 'admin_usuarios'
  get 'admin/tipos', to: 'admin#index', :as => 'admin_tipos'
  get 'admin/areas', to: 'admin#index', :as => 'admin_areas'
  get 'admin/actividad', to: 'admin#index', :as => 'admin_actividad'

  post 'admin/create_tipo', to: 'admin#create_tipo'
  post 'admin/update_tipo', to: 'admin#update_tipo'
  post 'admin/delete_tipo', to: 'admin#delete_tipo'

  post 'admin/create_area', to: 'admin#create_area'
  post 'admin/update_area', to: 'admin#update_area'
  post 'admin/delete_area', to: 'admin#delete_area'

  post 'admin/update_usuario', to: 'admin#update_usuario'
  get 'home', to: 'home#index'

  get 'search/advanced', to: 'busquedas#advanced'
  get 'results', to: 'busquedas#search'
  get 'search', to: 'busquedas#normal_search'

  post 'delete', to: 'documentos#delete'
  post 'edit', to: 'documentos#edit'
  post 'rewrite', to: 'documentos#rewrite'

  # Es necesario que la root esté seteada para el tema de usuarios
  root to: "home#index"
end
